import os
import shutil
import tempfile
import unittest
from contextlib import contextmanager

from treqs.main import main


class TestSystem(unittest.TestCase):
    def setUp(self):
        self.temp_directory = tempfile.mkdtemp()
        self.old_directory = os.getcwd()
        os.chdir(self.temp_directory)

    def tearDown(self):
        os.chdir(self.old_directory)
        shutil.rmtree(self.temp_directory)

    def test_logs_directory_is_created(self):
        """
        [testcase id=TC0001 story=US0002 requirement=REQ0005]

        Ensure that a logs/ directory is created in the current
        directory when treqs is run.
        """
        # Act
        main('treqs -u . -s . -t . -q'.split())

        # Assert
        self.assertTrue(os.path.isdir('logs'))

    def test_list_all_artefacts_in_folder(self):
        """
        [testcase id=TC0006 story=US0001 requirement=REQ0001,REQ0002,REQ0011]

        Ensure that all test cases, user stories and requirements
        retrieved by treqs are listed in the log files
        """
        # Arrange
        # Workaround due to https://github.com/regot-chalmers/treqs/issues/29
        with open('US_all.md', 'w+') as f:
            f.write('[user' + 'story id=UStemp]')
        with open('TC_all.md', 'w+') as f:
            f.write('[test' + 'case id=TCtemp]')
        with open('SR_all.md', 'w+') as f:
            f.write('[requir' + 'ement id=REQtemp]')

        # Act
        main('treqs -u . -s . -t . -q'.split())

        # Assert
        for start_of_filename, artefact_name in [('TC', 'TCtemp'),
                                                 ('SysReq', 'REQtemp'),
                                                 ('US', 'UStemp')]:
            with self.subTest(artefact_name):
                with _open_file_in_directory_starting_with('logs', start_of_filename) as f:
                    self.assertIn(artefact_name, f.read())

    def test_default_artefact_locations(self):
        """
        [testcase id=TC0007 story=US0001 requirement=REQ0012]

        Ensure that, if no arguments are specified, treqs
        looks in the default locations for artefacts 
        """
        # Arrange
        os.mkdir('requirements')
        os.mkdir('tests')

        # Arrange
        # Workaround due to https://github.com/regot-chalmers/treqs/issues/29
        with open('requirements/US_all.md', 'w+') as f:
            f.write('[user' + 'story id=UStemp]')
        with open('tests/TC_all.md', 'w+') as f:
            f.write('[test' + 'case id=TCtemp]')
        with open('requirements/SR_all.md', 'w+') as f:
            f.write('[requir' + 'ement id=REQtemp]')

        # Act
        main('treqs -q'.split())

        # Assert
        for start_of_filename, artefact_name in [('TC', 'TCtemp'),
                                                 ('SysReq', 'REQtemp'),
                                                 ('US', 'UStemp')]:
            with self.subTest(artefact_name):
                with _open_file_in_directory_starting_with('logs', start_of_filename) as f:
                    self.assertIn(artefact_name, f.read())

    def test_list_user_stories_without_requirements(self):
        """
        [testcase id=TC0005 story=US0001 requirement=REQ0003]

        Ensure that user stories without system requirements
        are listed in the Summary file
        """
        # Arrange
        with open('US_all.md', 'w+') as f:
            f.write('[userstory id=UStemp]')

        # Act
        main('treqs -u . -s . -t . -q'.split())

        # Assert
        with _open_file_in_directory_starting_with('logs', 'Summary') as f:
            self.assertIn('UStemp', f.read())


@contextmanager
def _open_file_in_directory_starting_with(directory, starts_with):
    filename = next(filter(lambda x: x.startswith(
        starts_with), os.listdir(directory)))
    file_path = os.path.join('logs', filename)
    with open(file_path) as f:
        yield f
