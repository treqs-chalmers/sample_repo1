# Context diagram

```puml
@startdot
digraph contextDiagram {
    node[shape="record"]
    subgraph externalEntities {
        entity1[label="Customer"];
        entity2[label="Manager"];
    }
    subgraph cluster_System {
        label="System environment";
        process1[label="System" shape="ellipse"];
        graph[style=dotted]
    }
    entity1->process1[label="Request for services"];
    process1->entity2[label="Reports"];
}
@enddot
```

## Data flow diagram

```puml
@startdot
digraph dfd {
    node[shape=record]
    store1 [label="<f0> 1|<f1> Data store"];
    entity1 [label="User" shape=box];
    process1 [label="Process" shape="ellipse"];
    store1:f1 -> process1[label="Username, password"];
    entity1-> process1[label="Authenticate"];
}
@enddot
```
