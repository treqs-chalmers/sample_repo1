# Meeting Scheduler System

System requirements for a an online food ordering system

## Detailed system requirements

The following requirements are derived from high-level system requirements

### [requirement id=RESR_0031 parent=REQ0002 quality=QR0205 test=TC0013]

The system must provide the ability to place an order, book a delivery with our partners to the user.

### [requirement id=REQ0008 parent=REQ0002]

The system should provide ability to send a confirmation by email.
